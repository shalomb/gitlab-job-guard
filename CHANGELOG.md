v0.0.4 - 2019-04-17T02:33:46
----------------------------

* Documentation improvements
* Extend test cases

v0.0.3 - 2019-04-16T23:43:44
----------------------------

* Minor Build/CI fixes relating to twine pypi package upload

v0.0.2 - 2019-04-16T18:29:44
----------------------------

* Add README.md
* Add e2e test suite
* Fix up CI build issues

v0.0.1 - 2019-04-15T20:17:13
----------------------------

* WIP - Intitial Concept


# Contributing

Contributions/patches and ideas are welcome!!

The [TODO](./README.md) requires help!!

## Merge Requests

Please create a
[merge-request](https://gitlab.com/s.bhooshi/gitlab-job-guard/merge_requests)
accompanied by a [suitable test](.gitlab-ci.yml#L45) (_a la_ TDD).

Ideally, the pipeline must pass before the merge-request is accepted.

## Functionality

* Please refer to the `TODO` section in [README.md](./README.md) to see if your
  case is not already covered.
* Please submit merge-request per feature/piece of functionality.

## Deployment Environment

* Target `python3` as the default Python version and maintain backwards
  compatibility with `python2`.
* Ensure `python2` (`2.7`) is supported until at least `31 March, 2020`.
  Please test this configuration, if unsupportable, the script must fail-early.

## Etiquette

* Honour the one change to one commit policy (your change must be revertable
  and not cause side-effects).
* Honour
  [well-formed git commit messages](https://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html)
  and commit history. Really, this is important!
* `git-flow` is the branching workflow for the project
  * `feature/` branches are advised for new functionality.
  * `hotfix/` branches are advised for patches/edits to existing
     [releases](https://gitlab.com/s.bhooshi/gitlab-job-guard/tags).
  * In all cases, `master` and `develop` cannot be pushed to directly and so
    merge-requests are needed.

